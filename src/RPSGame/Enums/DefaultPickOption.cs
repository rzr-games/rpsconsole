﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="DefaultPickOption.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

namespace RPSGame.Enums
{
    /// <summary>
    ///     Available pick options
    /// </summary>
    public enum DefaultPickOption
    {
        /// <summary>
        ///     ROCK
        /// </summary>
        Rock,

        /// <summary>
        ///     PAPER
        /// </summary>
        Paper,

        /// <summary>
        ///     SCISSORS
        /// </summary>
        Scissors,

        /// <summary>
        ///     FIRE
        /// </summary>
        Fire,

        /// <summary>
        ///     SPOCK
        /// </summary>
        Spock
    }
}