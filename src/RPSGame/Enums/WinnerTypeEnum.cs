﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="WinnerTypeEnum.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

// ReSharper disable InconsistentNaming

namespace RPSGame.Enums
{
    public enum WinnerTypeEnum
    {
        /// <summary>
        ///     Player win round
        /// </summary>
        Player,

        /// <summary>
        ///     CPU win
        /// </summary>
        CPU,

        /// <summary>
        ///     No one win round
        /// </summary>
        Tie
    }
}