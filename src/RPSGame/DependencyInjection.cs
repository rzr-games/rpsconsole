﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="DependencyInjection.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using Microsoft.Extensions.DependencyInjection;
using RPSGame.Abstractions;
using RPSGame.GameData;
using RPSGame.Services;
using UniqueServiceCollection;

#endregion

namespace RPSGame
{
    public static class DependencyInjection
    {
        public static void RegisterGameServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddUnique<IGameUi, ConsoleClientUi>(ServiceLifetime.Scoped);
            serviceCollection.AddUnique<IScoreboard, PlayerScoreboard>(ServiceLifetime.Scoped);

            serviceCollection.AddUnique<IPickOptionService, PickOptionService>(ServiceLifetime.Scoped);

            serviceCollection.AddUnique<IGame, Game>(ServiceLifetime.Scoped);
        }
    }
}