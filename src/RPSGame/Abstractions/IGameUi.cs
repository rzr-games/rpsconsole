﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="IGameUi.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using RPSGame.Helpers.Option;
using RPSGame.Helpers.Rules;

#endregion

namespace RPSGame.Abstractions
{
    /// <summary>
    ///     Game UI dialog
    /// </summary>
    public interface IGameUi
    {
        /// <summary>
        ///     Show a message asking for the player's name
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        string AskForPlayerName();

        /// <summary>
        ///     Show a message asking to select the option
        /// </summary>
        /// <param name="options">Available game option</param>
        /// <returns></returns>
        /// <remarks></remarks>
        GameOption AskForOption(GameOption[] options);

        /// <summary>
        ///     Show TIE round message
        /// </summary>
        /// <remarks></remarks>
        void DisplayTieMessage();

        /// <summary>
        ///     Show winner message
        /// </summary>
        /// <param name="message"></param>
        /// <remarks></remarks>
        void DisplayWinMessage(string message);

        /// <summary>
        ///     Show message with choices
        /// </summary>
        /// <param name="playerName">Player name</param>
        /// <param name="cpuName">CPU name</param>
        /// <param name="playerOption">Player selected option</param>
        /// <param name="cpuOption">CPU selected option</param>
        /// <remarks></remarks>
        void DisplayRoundChoices(string playerName, string cpuName, GameOption playerOption, GameOption cpuOption);

        /// <summary>
        ///     Show current score
        /// </summary>
        /// <param name="playerName">Required. Player name</param>
        /// <param name="cpuName">Required. CPU name</param>
        /// <param name="playerScore">Optional. Player score. The default value is 0.</param>
        /// <param name="cpuScore">Optional. CPU score. The default value is 0.</param>
        /// <remarks></remarks>
        void ShowScore(string playerName, string cpuName, int playerScore = 0, int cpuScore = 0);

        /// <summary>
        ///     Show welcome player message
        /// </summary>
        /// <param name="name"></param>
        /// <remarks></remarks>
        void WelcomePlayer(string name);

        /// <summary>
        ///     Show initial message
        /// </summary>
        /// <remarks></remarks>
        void InitialMessage();

        /// <summary>
        ///     Show game rules
        /// </summary>
        /// <param name="rules">Gama rules</param>
        /// <remarks></remarks>
        void ShowRules(Rule[] rules);

        /// <summary>
        ///     Show wrong input
        /// </summary>
        /// <remarks></remarks>
        void DisplayWrongInputMessage();

        /// <summary>
        ///     Show final score
        /// </summary>
        /// <param name="playerName">Required. Player name</param>
        /// <param name="cpuName">Required. CPU name</param>
        /// <param name="playerScore">Optional. Player score. The default value is 0.</param>
        /// <param name="cpuScore">Optional. CPU score. The default value is 0.</param>
        void ShowFinalScore(string playerName, string cpuName, int playerScore = 0, int cpuScore = 0);
    }
}