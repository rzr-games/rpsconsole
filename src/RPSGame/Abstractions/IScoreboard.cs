﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="IScoreboard.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System.Collections.Generic;
using RPSGame.Models;

#endregion

namespace RPSGame.Abstractions
{
    /// <summary>
    ///     Game scoreboard
    /// </summary>
    public interface IScoreboard
    {
        /// <summary>
        ///     Get score
        /// </summary>
        /// <param name="playerName">Player name</param>
        /// <param name="score">Player score</param>
        /// <remarks></remarks>
        void SetScore(string playerName, int score);

        /// <summary>
        ///     Get player score
        /// </summary>
        /// <param name="playerName">Player name</param>
        /// <returns>Return player score</returns>
        /// <remarks></remarks>
        int GetScore(string playerName);

        /// <summary>
        ///     Get all scores
        /// </summary>
        /// <returns>Get all players score</returns>
        /// <remarks></remarks>
        IEnumerable<ScoreEntry> GetAllScores();

        /// <summary>
        ///     Increase player score
        /// </summary>
        /// <param name="playerName">Player name</param>
        /// <remarks></remarks>
        void IncreaseScore(string playerName);

        /// <summary>
        ///     Decrease player score
        /// </summary>
        /// <param name="playerName">Player name</param>
        /// <remarks></remarks>
        void DecreaseScore(string playerName);
    }
}