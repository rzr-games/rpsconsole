﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="IPlayerScoreboardScores.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System.Collections.Generic;

#endregion

namespace RPSGame.Abstractions
{
    /// <summary>
    ///     Player score board
    /// </summary>
    public interface IPlayerScoreboardScores
    {
        /// <summary>
        ///     Players score
        /// </summary>
        Dictionary<string, int> PlayersScore { get; }
    }
}