﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="IGamePlayer.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

namespace RPSGame.Abstractions
{
    /// <summary>
    ///     Game player
    /// </summary>
    public interface IGamePlayer
    {
        /// <summary>
        ///     Player name
        /// </summary>
        string PlayerName { get; }
    }
}