﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="IGame.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System.Threading;
using System.Threading.Tasks;
using RPSGame.Helpers.Rules;

#endregion

namespace RPSGame.Abstractions
{
    /// <summary>
    ///     Game
    /// </summary>
    public interface IGame
    {
        /// <summary>
        ///     Start new game
        /// </summary>
        /// <returns></returns>
        Task StartAsync();

        /// <summary>
        ///     Set game rules
        /// </summary>
        /// <param name="ruleSet">Game rules</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns></returns>
        Task SetRules(RuleSet ruleSet, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Set number of rounds
        /// </summary>
        /// <param name="numberOfRounds">Number of rounds, default = 3</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns></returns>
        Task SetNumberOfRoundsAsync(int numberOfRounds = 3, CancellationToken cancellationToken = default);
    }
}