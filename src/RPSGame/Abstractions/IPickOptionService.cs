﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="IPickOptionService.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System.Threading;
using System.Threading.Tasks;
using RPSGame.Helpers.Option;

#endregion

namespace RPSGame.Abstractions
{
    /// <summary>
    ///     Pick option service
    /// </summary>
    /// <remarks></remarks>
    public interface IPickOptionService
    {
        /// <summary>
        ///     Pick random option from available option
        /// </summary>
        /// <param name="gameOptions">Required. All game option</param>
        /// <param name="cancellationToken">Optional. The default value is default.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        Task<GameOption> PickRandomOptionAsync(
            GameOption[] gameOptions,
            CancellationToken cancellationToken = default);
    }
}