﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="ScoreEntry.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

// ReSharper disable ClassNeverInstantiated.Global

namespace RPSGame.Models
{
    /// <summary>
    ///     Score entry
    /// </summary>
    public class ScoreEntry
    {
        /// <summary>
        ///     Player name
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        ///     Player score
        /// </summary>
        public int Score { get; set; }
    }
}