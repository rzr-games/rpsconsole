﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="ConsoleClientUi.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System;
using System.Linq;
using RPSGame.Abstractions;
using RPSGame.Enums;
using RPSGame.Helpers;
using RPSGame.Helpers.Option;
using RPSGame.Helpers.Rules;

#endregion

namespace RPSGame
{
    /// <inheritdoc cref="IGameUi" />
    public class ConsoleClientUi : IGameUi
    {
        /// <inheritdoc />
        public string AskForPlayerName()
        {
            Console.WriteLine(MessageBuilder.BuildAskForPlayerNameMessage());

            var playerName = Console.ReadLine();
            //while (string.IsNullOrEmpty(playerName))
            //{
            //    MessageBuilder.BuildWrongInputMessage();
            //}

            return playerName;
        }

        /// <inheritdoc />
        public GameOption AskForOption(GameOption[] options)
        {
            Console.WriteLine(MessageBuilder.BuildAskForOptionMessage(options));
            var playerOption = Console.ReadLine();

            var availablePickOption = Enum.GetNames(typeof(DefaultPickOption)).Select((t, i) => i).ToList();
            if (string.IsNullOrEmpty(playerOption)
                || int.TryParse(playerOption, out _) == false
                || availablePickOption.All(x => x != int.Parse(playerOption)))
                return null;

            return options[int.Parse(playerOption)];
        }

        /// <inheritdoc />
        public void DisplayTieMessage()
            => Console.WriteLine(MessageBuilder.BuildTieMessage());

        /// <inheritdoc />
        public void DisplayWinMessage(string message)
            => Console.WriteLine(MessageBuilder.BuildWinMessage(message));

        /// <inheritdoc />
        public void DisplayRoundChoices(string playerName, string cpuName, GameOption playerOption, GameOption cpuOption)
            => Console.WriteLine(MessageBuilder.BuildRoundChoicesMessage(playerName, cpuName, playerOption, cpuOption));

        /// <inheritdoc />
        public void ShowScore(string playerName, string cpuName, int playerScore = 0, int cpuScore = 0)
            => Console.WriteLine(MessageBuilder.BuildShowScoreMessage(playerName, cpuName, playerScore, cpuScore));

        /// <inheritdoc />
        public void WelcomePlayer(string name)
            => Console.WriteLine(MessageBuilder.BuildWelcomeMessage(name));

        /// <inheritdoc />
        public void InitialMessage()
            => Console.WriteLine(MessageBuilder.BuildInitialMessage());

        /// <inheritdoc />
        public void ShowRules(Rule[] rules)
            => Console.WriteLine(MessageBuilder.BuildShowRulesMessage(rules));

        /// <inheritdoc />
        public void DisplayWrongInputMessage()
            => Console.WriteLine(MessageBuilder.BuildWrongInputMessage());

        /// <inheritdoc />
        public void ShowFinalScore(string playerName, string cpuName, int playerScore = 0, int cpuScore = 0)
            => Console.WriteLine(MessageBuilder.BuildGameOverMessage(playerName, cpuName, playerScore, cpuScore));
    }
}