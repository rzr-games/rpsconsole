﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="PlayerScoreboard.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System.Collections.Generic;
using System.Linq;
using RPSGame.Abstractions;
using RPSGame.Extensions;
using RPSGame.Models;

// ReSharper disable ClassNeverInstantiated.Global

#endregion

namespace RPSGame.GameData
{
    /// <inheritdoc cref="IPlayerScoreboardScores" />
    public class PlayerScoreboard : IScoreboard, IPlayerScoreboardScores
    {
        /// <inheritdoc />
        public Dictionary<string, int> PlayersScore { get; }
            = new Dictionary<string, int>();

        /// <inheritdoc />
        public void SetScore(string playerName, int score)
            => PlayersScore[playerName] = score;

        /// <inheritdoc />
        public int GetScore(string playerName)
            => PlayersScore[playerName].IsNull() ? 0 : PlayersScore[playerName];

        /// <inheritdoc />
        public IEnumerable<ScoreEntry> GetAllScores()
            => PlayersScore.Select(x => new ScoreEntry { PlayerName = x.Key, Score = x.Value });

        /// <inheritdoc />
        public void IncreaseScore(string playerName)
            => PlayersScore[playerName] += 1;

        /// <inheritdoc />
        public void DecreaseScore(string playerName)
            => PlayersScore[playerName] -= 1;
    }
}