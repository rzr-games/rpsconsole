﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="GamePlayer.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using RPSGame.Abstractions;

#endregion

namespace RPSGame.GameData
{
    /// <inheritdoc cref="IGamePlayer" />
    public class GamePlayer : IGamePlayer
    {
        public GamePlayer(string playerName) => PlayerName = playerName;

        /// <inheritdoc />
        public string PlayerName { get; }
    }
}