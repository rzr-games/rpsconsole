﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="PickOptionService.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using RPSGame.Abstractions;
using RPSGame.Helpers.Option;

// ReSharper disable ClassNeverInstantiated.Global

#endregion

namespace RPSGame.Services
{
    /// <inheritdoc cref="IPickOptionService" />
    public class PickOptionService : IPickOptionService
    {
        /// <inheritdoc />
        public async Task<GameOption> PickRandomOptionAsync(
            GameOption[] gameOptions,
            CancellationToken cancellationToken = default)
        {
            var randomIndex = Next(0, 4);

            return await Task.FromResult(gameOptions[randomIndex]);
        }

        /// <summary>
        ///     Get next random number
        /// </summary>
        /// <param name="min">Minim/Start value</param>
        /// <param name="max">Maximum/End value</param>
        /// <returns>An integer value from specifier range.</returns>
        /// <remarks></remarks>
        private static int Next(int min, int max)
        {
            if (min > max)
                throw new ArgumentOutOfRangeException(nameof(min));
            if (min == max)
                return min;

            using (var rng = new RNGCryptoServiceProvider())
            {
                var data = new byte[4];
                rng.GetBytes(data);

                var generatedValue = Math.Abs(BitConverter.ToInt32(data, 0));

                var diff = max - min;
                var mod = generatedValue % diff;
                var normalizedNumber = min + mod;

                return normalizedNumber;
            }
        }
    }
}