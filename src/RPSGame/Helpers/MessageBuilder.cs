﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="MessageBuilder.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System.Collections.Generic;
using System.Linq;
using System.Text;
using RPSGame.Helpers.Option;
using RPSGame.Helpers.Rules;

#endregion

namespace RPSGame.Helpers
{
    /// <summary>
    ///     Message builder
    /// </summary>
    public static class MessageBuilder
    {
        private const string LineDivider = "-----------------------------------------";

        /// <summary>
        ///     Build initial message
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string BuildInitialMessage()
            => new StringBuilder()
                .AppendLine("This is rock paper scissors and fire!")
                .AppendLine("You will be playing against 'Bob', as that's your opponent's name.\n")
                .ToString();

        /// <summary>
        ///     Build player name message
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string BuildAskForPlayerNameMessage()
            => new StringBuilder().Append("Please enter your name:").ToString();

        /// <summary>
        ///     Build TIE round message
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string BuildTieMessage() => new StringBuilder()
            .AppendLine(LineDivider)
            .AppendLine("This round was a tie!")
            .AppendLine(LineDivider)
            .ToString();

        /// <summary>
        ///     Build welcome player message
        /// </summary>
        /// <param name="playerName">Current player name</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string BuildWelcomeMessage(string playerName)
            => new StringBuilder()
                .AppendLine(LineDivider)
                .AppendLine($"Welcome {playerName}, the game will start.")
                .ToString();

        /// <summary>
        ///     Build round win message
        /// </summary>
        /// <param name="winnerName">Winner name</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string BuildWinMessage(string winnerName)
            => new StringBuilder()
                .AppendLine(LineDivider)
                .AppendLine($"This round, {winnerName} has won!")
                .ToString();

        /// <summary>
        ///     Build message with selected option by player and CPU
        /// </summary>
        /// <param name="playerName">Required. Player name.</param>
        /// <param name="cpuName">Required. CPU name.</param>
        /// <param name="playerOption">layer selected option</param>
        /// <param name="cpuOption">CPU selected option</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string BuildRoundChoicesMessage(string playerName, string cpuName, GameOption playerOption, GameOption cpuOption)
            => new StringBuilder()
                .AppendLine(LineDivider)
                .AppendLine($"{playerName} has chosen: {playerOption.GetLabel()}")
                .AppendLine($"{cpuName} has chosen: {cpuOption.GetLabel()}")
                .AppendLine(LineDivider)
                .ToString();

        /// <summary>
        ///     Build message with player score
        /// </summary>
        /// <param name="playerName">Required. Player name.</param>
        /// <param name="cpuName">Required. CPU</param>
        /// <param name="playerScore">Optional. Player score. The default value is 0.</param>
        /// <param name="cpuScore">Optional. CPU score. The default value is 0.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string BuildShowScoreMessage(string playerName, string cpuName, int playerScore = 0, int cpuScore = 0)
            => new StringBuilder()
                .AppendLine($"Scores: {playerName}: {playerScore} | {cpuName}: {cpuScore}")
                .ToString();

        /// <summary>
        ///     Build game rules message
        /// </summary>
        /// <param name="rules"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string BuildShowRulesMessage(IEnumerable<Rule> rules)
        {
            var sb = new StringBuilder();
            sb.AppendLine("These are the rules:");
            foreach (var rule in rules)
                sb.AppendLine($"\t* {rule.WinnerOption.GetLabel()} wins over {string.Join(", and ", rule.LooserOption.Select(x => x.GetLabel()))}");

            return sb.ToString();
        }

        /// <summary>
        ///     Build message with available options
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string BuildAskForOptionMessage(GameOption[] options)
        {
            var sb = new StringBuilder()
                .AppendLine(LineDivider)
                .AppendLine("Select one of the below options:");

            for (var i = 0; i < options.Length; i++)
                sb.AppendLine($"\t{i} => {options[i].GetLabel()}");

            return sb.ToString();
        }

        /// <summary>
        ///     Build wrong input message
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string BuildWrongInputMessage()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Please choose a valid option.");

            return sb.ToString();
        }

        /// <summary>
        ///     Build game over message with final score
        /// </summary>
        /// <param name="playerName">Required. Player name.</param>
        /// <param name="cpuName">Required. CPU</param>
        /// <param name="playerScore">Optional. Player score. The default value is 0.</param>
        /// <param name="cpuScore">Optional. CPU score. The default value is 0.</param>
        /// <returns></returns>
        public static string BuildGameOverMessage(string playerName, string cpuName, int playerScore = 0, int cpuScore = 0)
        {
            var sb = new StringBuilder()
                .AppendLine(LineDivider)
                .AppendLine("|-\t\t\t\t\t-|")
                .AppendLine("|-\t\tGAME OVER!\t\t-|")
                .AppendLine("|-\t\tFinal score:\t\t-|")
                .AppendLine("|----------------------------------------|")
                .AppendLine($"\t{playerName}: {playerScore} | {cpuName}: {cpuScore}")
                .AppendLine(LineDivider);

            return sb.ToString();
        }
    }
}