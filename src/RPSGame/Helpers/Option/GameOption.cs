﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="GameOption.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using RPSGame.Enums;
using RPSGame.Helpers.Rules;

#endregion

namespace RPSGame.Helpers.Option
{
    /// <summary>
    ///     Game option
    /// </summary>
    public class GameOption
    {
        /// <summary>
        ///     Default available option
        /// </summary>
        private readonly DefaultPickOption _option;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RPSGame.Helpers.Option.GameOption" /> class.
        /// </summary>
        /// <param name="option">Available game option</param>
        /// <remarks></remarks>
        public GameOption(DefaultPickOption option) => _option = option;

        /// <summary>
        ///     Game option winner rule
        /// </summary>
        /// <param name="looser">Looser options</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public Rule WinsOver(params GameOption[] looser) => new Rule(this, looser);

        /// <summary>
        ///     Get option label
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public DefaultPickOption GetLabel() => _option;
    }
}