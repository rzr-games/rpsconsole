﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="RuleSet.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System.Linq;
using RPSGame.Enums;
using RPSGame.Helpers.Option;

#endregion

namespace RPSGame.Helpers.Rules
{
    /// <summary>
    ///     Game rule setter
    /// </summary>
    public class RuleSet
    {
        /// <summary>
        ///     Game options
        /// </summary>
        private readonly GameOption[] _gameOptions;

        /// <summary>
        ///     Rules
        /// </summary>
        private Rule[] _rules;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RPSGame.Helpers.Rules.RuleSet" /> class.
        /// </summary>
        /// <param name="gameOptions">Available game options</param>
        /// <remarks></remarks>
        public RuleSet(params GameOption[] gameOptions)
        {
            _gameOptions = gameOptions;
            _rules = new Rule[] { };
        }

        /// <summary>
        ///     Add new game rules
        /// </summary>
        /// <param name="rules">Game rules</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public RuleSet AddRules(params Rule[] rules)
        {
            _rules = _rules.Concat(rules).ToArray();

            return this;
        }

        /// <summary>
        ///     Get available game rules
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public Rule[] GetRules()
            => _rules;

        /// <summary>
        ///     Get available game options
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public GameOption[] GetOptions()
            => _gameOptions;

        /// <summary>
        ///     Get winner option
        /// </summary>
        /// <param name="playerOption">PLAYER -> Game option</param>
        /// <param name="cpuOption">CPU -> Game option</param>
        /// <returns>Return winner game option</returns>
        /// <remarks></remarks>
        public WinnerTypeEnum WhoWins(GameOption playerOption, GameOption cpuOption)
        {
            if (playerOption == cpuOption)
                return WinnerTypeEnum.Tie;

            foreach (var rule in _rules)
            {
                var winOpt1 = rule.IsWinner(playerOption, cpuOption);
                if (winOpt1)
                    return WinnerTypeEnum.Player;

                var winOpt2 = rule.IsWinner(cpuOption, playerOption);
                if (winOpt2)
                    return WinnerTypeEnum.CPU;
            }

            return WinnerTypeEnum.Tie;
        }
    }
}