﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="DefaultRules.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using RPSGame.Enums;
using RPSGame.Helpers.Option;

#endregion

// ReSharper disable ClassNeverInstantiated.Global

namespace RPSGame.Helpers.Rules
{
    /// <summary>
    ///     Predefined rules
    /// </summary>
    public class DefaultRules
    {
        /// <summary>
        ///     Default rules
        /// </summary>
        public readonly RuleSet DefRuleSet;

        private readonly GameOption fire = new GameOption(DefaultPickOption.Fire);
        private readonly GameOption paper = new GameOption(DefaultPickOption.Paper);
        private readonly GameOption rock = new GameOption(DefaultPickOption.Rock);
        private readonly GameOption scissors = new GameOption(DefaultPickOption.Scissors);
        private readonly GameOption spock = new GameOption(DefaultPickOption.Spock);

        /// <summary>
        ///     Initializes a new instance of the <see cref="RPSGame.Helpers.Rules.DefaultRules" /> class.
        /// </summary>
        /// <remarks></remarks>
        private DefaultRules()
        {
            DefRuleSet = new RuleSet(rock, paper, scissors, fire, spock);

            DefRuleSet.AddRules(
                rock.WinsOver(scissors, fire, spock),
                spock.WinsOver(fire),
                paper.WinsOver(rock, spock),
                scissors.WinsOver(paper, spock),
                fire.WinsOver(paper, scissors));
        }

        /// <summary>
        ///     Instance
        /// </summary>
        public static DefaultRules Instance => new DefaultRules();
    }
}