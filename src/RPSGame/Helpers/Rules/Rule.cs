﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="Rule.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System.Linq;
using RPSGame.Helpers.Option;

#endregion

namespace RPSGame.Helpers.Rules
{
    /// <summary>
    ///     Rule
    /// </summary>
    public class Rule
    {
        /// <summary>
        ///     Looser options
        /// </summary>
        public readonly GameOption[] LooserOption;

        /// <summary>
        ///     Winner option
        /// </summary>
        public readonly GameOption WinnerOption;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RPSGame.Helpers.Rules.Rule" /> class.
        /// </summary>
        /// <param name="winnerOption">Winner option</param>
        /// <param name="looserOption">Looser options</param>
        /// <remarks></remarks>
        public Rule(GameOption winnerOption, params GameOption[] looserOption)
        {
            WinnerOption = winnerOption;
            LooserOption = looserOption;
        }

        /// <summary>
        ///     Check if option is winner
        /// </summary>
        /// <param name="winnerOption">Winner option</param>
        /// <param name="looserOption">Looser option</param>
        /// <returns>Return if <see cref="winnerOption" /> wins over <see cref="looserOption" /></returns>
        /// <remarks></remarks>
        public bool IsWinner(GameOption winnerOption, GameOption looserOption)
            => WinnerOption == winnerOption && LooserOption.Contains(looserOption);
    }
}