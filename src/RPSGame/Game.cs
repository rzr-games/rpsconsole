﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.RPSGame
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="Game.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System;
using System.Threading;
using System.Threading.Tasks;
using RPSGame.Abstractions;
using RPSGame.Enums;
using RPSGame.Extensions;
using RPSGame.Helpers.Rules;

#endregion

namespace RPSGame
{
    public class Game : IGame
    {
        private const string CPU = "CPU";
        private static RuleSet _ruleSet;
        private static string _playerName = "Human";
        private static int _playedRounds;
        private static int _rounds;
        private readonly IGameUi _gameUi;
        private readonly IPickOptionService _pickOptionService;
        private readonly IScoreboard _scoreboard;

        public Game(IGameUi gameUi, IPickOptionService pickOptionService, IScoreboard scoreboard)
        {
            _gameUi = gameUi;
            _pickOptionService = pickOptionService;
            _scoreboard = scoreboard;

            //Set default initialize value
            _ruleSet = _ruleSet ?? DefaultRules.Instance.DefRuleSet;
            _rounds = 3;
        }

        public async Task StartAsync()
        {
            _gameUi.InitialMessage();

            var playerName = _gameUi.AskForPlayerName();
            if (!string.IsNullOrEmpty(playerName) && !string.IsNullOrWhiteSpace(playerName))
                _playerName = playerName;

            _gameUi.WelcomePlayer(_playerName);

            _gameUi.ShowRules(_ruleSet.GetRules());

            _scoreboard.SetScore(_playerName, 0);
            _scoreboard.SetScore(CPU, 0);

            while (_playedRounds < _rounds)
                await PlayRound();

            if (_playedRounds == _rounds)
                _gameUi.ShowFinalScore(_playerName, CPU, _scoreboard.GetScore(_playerName), _scoreboard.GetScore(CPU));
        }
        
        /// <inheritdoc />
        public async Task SetRules(RuleSet ruleSet, CancellationToken cancellationToken = default)
        {
            if (ruleSet != null)
                _ruleSet = ruleSet;

            await Task.CompletedTask;
        }

        /// <inheritdoc />
        public async Task SetNumberOfRoundsAsync(int numberOfRounds = 3, CancellationToken cancellationToken = default)
            => await Task.FromResult(_rounds = numberOfRounds);

        private async Task PlayRound()
        {
            try
            {
                var userInput = _gameUi.AskForOption(_ruleSet.GetOptions());
                if (userInput.IsNull())
                {
                    _gameUi.DisplayWrongInputMessage();

                    return;
                }
                
                var cpuInput = await _pickOptionService.PickRandomOptionAsync(_ruleSet.GetOptions());

                _gameUi.DisplayRoundChoices(_playerName, CPU, userInput, cpuInput);

                var winner = _ruleSet.WhoWins(userInput, cpuInput);

                switch (winner)
                {
                    case WinnerTypeEnum.Player:
                        _gameUi.DisplayWinMessage(_playerName);
                        _scoreboard.IncreaseScore(_playerName);

                        _gameUi.ShowScore(_playerName, CPU, _scoreboard.GetScore(_playerName), _scoreboard.GetScore(CPU));
                        _playedRounds++;
                        break;
                    case WinnerTypeEnum.CPU:

                        _gameUi.DisplayWinMessage(CPU);
                        _scoreboard.IncreaseScore(CPU);

                        _gameUi.ShowScore(_playerName, CPU, _scoreboard.GetScore(_playerName), _scoreboard.GetScore(CPU));
                        _playedRounds++;
                        break;
                    default:
                        _gameUi.DisplayTieMessage();
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                _gameUi.DisplayWrongInputMessage();
            }
        }
    }
}