﻿// ***********************************************************************
//  Assembly         : RzR.Shared.Games.GameConsoleTest
//  Author           : RzR
//  Created On       : 2023-06-01 22:14
// 
//  Last Modified By : RzR
//  Last Modified On : 2023-06-01 22:28
// ***********************************************************************
//  <copyright file="Program.cs" company="">
//   Copyright (c) RzR. All rights reserved.
//  </copyright>
// 
//  <summary>
//  </summary>
// ***********************************************************************

#region U S A G E S

using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using RPSGame;
using RPSGame.Abstractions;
using RPSGame.Helpers.Rules;

#endregion

namespace GameConsoleTest
{
    internal class Program
    {
        private static Task Main(string[] args)
        {
            return new Program().MainAsync();
        }

        private async Task MainAsync()
        {
            // Create service collection
            var serviceCollection = (IServiceCollection) new ServiceCollection();
            serviceCollection.RegisterGameServices();
            var services = serviceCollection.BuildServiceProvider();

            var pickOptionService = services.GetRequiredService<IPickOptionService>();
            var scoreBoardService = services.GetRequiredService<IScoreboard>();
            var uiGame = new ConsoleClientUi();

            var game = new Game(uiGame, pickOptionService, scoreBoardService);

            await game.SetRules(DefaultRules.Instance.DefRuleSet);
            await game.SetNumberOfRoundsAsync(5);
            await game.StartAsync();
        }
    }
}